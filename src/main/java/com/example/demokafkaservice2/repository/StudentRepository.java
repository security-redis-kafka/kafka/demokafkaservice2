package com.example.demokafkaservice2.repository;

import com.example.demokafkaservice2.entity.Students;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Students, Integer> {
}
