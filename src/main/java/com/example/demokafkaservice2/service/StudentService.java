package com.example.demokafkaservice2.service;

import com.example.demokafkaservice2.entity.Students;
import com.example.demokafkaservice2.repository.StudentRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class StudentService {
    private final StudentRepository repository;
    private final ObjectMapper objectMapper;

    @KafkaListener(topics = "${application.kafka.topic.student}", groupId = "myGroup")
    private void insert(String studentJson) {
        try {
            Students student = objectMapper.readValue(studentJson, Students.class);
            repository.save(student);
            log.info("Insert Student Success " + student);
        } catch (JsonProcessingException e) {
            log.error("[ERROR] Something went wrong: {}", e.getMessage());
        }
    }
}
