package com.example.demokafkaservice2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoKafkaService2Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoKafkaService2Application.class, args);
	}

}
